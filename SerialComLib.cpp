#include "Arduino.h"
#include "SerialComLib.h"

SerialComLib::SerialCom(Serial serial, char term, int del) {
    _serial = serial;
    _term = term;
    _del = del;
}

void SerialComLib::send(String msg) {
    delay(_del);
    _serial.print(msg + _term);
}

String SerialComLib::receive() {
    String s = "";
     if (_serial.available() > 0) {
    s = _serial.readStringUntil(_term);
  }
}
