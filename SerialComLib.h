/*
  SerialComLib.h - Library for serial communication
  Created by Giulio Cocconi, December 29, 2020.
  Released into the public domain.
*/
#ifndef SerialComLib_h
#define SerialComLib_h

#include "Arduino.h"


class SerialCom
{
  public:
    SerialCom(Serial serial, char term, int del);
    void send(String msg);
    String receive();
  private:
    Serial _serial;
    char _term;
    int _del;
};

#endif
